package com.project.testproject.app;

import com.project.testproject.mvp.models.General;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by onuchin on 19.11.2016.
 */

public interface OpenApi {

    @GET("api/countries")

    Call<General> getCountry();
}
