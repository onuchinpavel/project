package com.project.testproject.mvp.models;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by onuchin on 19.11.2016.
 */

public class Artists implements Parcelable {
    @SerializedName("Id")
    @Expose
     private Integer id;
    @SerializedName("CityId")
    @Expose
    private Intent  cityId;
    @SerializedName("Age")
    @Expose
    private  Integer age;
    @SerializedName("FirstName")
    @Expose
    private  String firstName;
    @SerializedName("LastName")
    @Expose
    private  String lastName;
    @SerializedName("Description")
    @Expose
    private  String desc;
    @SerializedName("Email")
    @Expose
    private  String email;
    @SerializedName("Phone")
    @Expose
    private  String phone;
    @SerializedName("Viber")
    @Expose
    private  String viber;
    private List<Images> images = new ArrayList<Images>();


    public Artists() {
    }

    protected Artists(Parcel in) {
        cityId = in.readParcelable(Intent.class.getClassLoader());
        firstName = in.readString();
        lastName = in.readString();
        desc = in.readString();
        email = in.readString();
        phone = in.readString();
        viber = in.readString();
        images = in.createTypedArrayList(Images.CREATOR);
    }

    public static final Creator<Artists> CREATOR = new Creator<Artists>() {
        @Override
        public Artists createFromParcel(Parcel in) {
            return new Artists(in);
        }

        @Override
        public Artists[] newArray(int size) {
            return new Artists[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(cityId, i);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(desc);
        parcel.writeString(email);
        parcel.writeString(phone);
        parcel.writeString(viber);
        parcel.writeTypedList(images);
    }
}
