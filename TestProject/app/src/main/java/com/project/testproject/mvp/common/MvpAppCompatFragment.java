package com.project.testproject.mvp.common;


import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.arellomobile.mvp.MvpDelegate;

/**
 * Created by onuchin on 17.11.2016.
 */

public class MvpAppCompatFragment extends Fragment {

    private MvpDelegate<? extends  MvpAppCompatFragment> mMvpDelegate;
    public MvpDelegate getMvpDelegate(){
        if(mMvpDelegate==null){
            mMvpDelegate = new MvpDelegate<>(this);
        }
        return  mMvpDelegate;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMvpDelegate().onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        getMvpDelegate().onAttach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(isRemoving()){
            getMvpDelegate().onDestroy();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getMvpDelegate().onSaveInstanceState(outState);
    }
}
