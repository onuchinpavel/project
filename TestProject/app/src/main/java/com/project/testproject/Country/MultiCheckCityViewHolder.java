package com.project.testproject.Country;

import android.view.View;
import android.widget.Checkable;
import android.widget.CheckedTextView;

import com.project.testproject.R;
import com.thoughtbot.expandablecheckrecyclerview.viewholders.CheckableChildViewHolder;

/**
 * Created by onuchin on 19.11.2016.
 */

public class MultiCheckCityViewHolder extends CheckableChildViewHolder {
     private CheckedTextView checkedTextView;

    public MultiCheckCityViewHolder(View itemView){
        super(itemView);
        checkedTextView =(CheckedTextView) itemView.findViewById(R.id.list_item_multicheck_artist_name);
    }
    @Override
    public Checkable getCheckable() {
        return checkedTextView;
    }
    public void setCityName(String cityName){
        checkedTextView.setText(cityName);
    }
}
