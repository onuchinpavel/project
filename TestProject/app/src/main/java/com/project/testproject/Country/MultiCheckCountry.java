package com.project.testproject.Country;

import com.project.testproject.mvp.models.City;
import com.thoughtbot.expandablecheckrecyclerview.models.MultiCheckExpandableGroup;

import java.util.List;

/**
 * Created by onuchin on 19.11.2016.
 */

public class MultiCheckCountry extends MultiCheckExpandableGroup {
    private  String name;

    public MultiCheckCountry(String title, List<City> items/*, String name*/) {
        super(title, items);
        this.name=name;
    }


    public  String getName(){
        return  name;
    }
}
