package com.project.testproject.mvp.presenters;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.project.testproject.app.OpenApi;
import com.project.testproject.app.OpenApiService;
import com.project.testproject.app.TestProjectApp;
import com.project.testproject.mvp.common.Utils;
import com.project.testproject.mvp.models.General;
import com.project.testproject.mvp.views.CountryView;

import javax.inject.Inject;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by onuchin on 19.11.2016.
 */
@InjectViewState
public class CountryPresenter extends MvpPresenter<CountryView> {
@Inject
    Context context;
    @Inject
    OpenApiService openApiService;

    public CountryPresenter() {
        TestProjectApp.getAppComponent().inject(this);

    }

    public void requestListcountry(){
        getViewState().showProgress();

        Observable<General> generalObservable = Utils.wrapRetrofitCall(openApiService.getCountry());
        Utils.wrapAsync(generalObservable, Schedulers.io())
                .subscribe(general -> {
                    getViewState().hideProgress();
                    getViewState().successRequest();
                },
                        throwable -> {
                            getViewState().hideProgress();
                            getViewState().showError(throwable.getMessage());
                        });
    }

    public void onErrorCancel(){getViewState().hideError();}
}
