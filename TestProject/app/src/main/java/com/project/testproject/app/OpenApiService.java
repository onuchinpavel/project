package com.project.testproject.app;

import com.project.testproject.mvp.models.General;

import retrofit2.Call;

/**
 * Created by onuchin on 19.11.2016.
 */

public class OpenApiService {
    private OpenApi mOpenApi;

    public OpenApiService(OpenApi mOpenApi) {
        this.mOpenApi = mOpenApi;
    }

   public Call<General> getCountry(){
        return  mOpenApi.getCountry();
    }
}
