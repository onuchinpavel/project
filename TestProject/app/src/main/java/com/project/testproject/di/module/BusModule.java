package com.project.testproject.di.module;

import com.project.testproject.app.OpenApi;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by onuchin on 19.11.2016.
 */

@Module
public class BusModule {
@Provides
    @Singleton
    public Bus provideBus(OpenApi openApi){
    return new Bus();
}
}
