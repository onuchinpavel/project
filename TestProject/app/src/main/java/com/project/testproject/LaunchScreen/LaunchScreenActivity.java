package com.project.testproject.LaunchScreen;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;


import com.project.testproject.MainActivity;
import com.project.testproject.R;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by onuchin on 17.11.2016.
 */

public class LaunchScreenActivity extends AppCompatActivity {

    public AnimationDrawable animationDrawable;
    public RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launch_screen_activity);

        relativeLayout = (RelativeLayout) findViewById(R.id.rLayout);
        animationDrawable = (AnimationDrawable) relativeLayout.getBackground();
        animationDrawable.setEnterFadeDuration(5000);
        animationDrawable.setExitFadeDuration(2000);
        new Timer().schedule(new TimerTask(){
            public void run() {
                startActivity(new Intent(LaunchScreenActivity.this, MainActivity.class));
            }
        }, 5000);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning())
            animationDrawable.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning())
            animationDrawable.stop();
    }

}
