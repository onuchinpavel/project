package com.project.testproject.Country;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.project.testproject.R;
import com.project.testproject.mvp.models.City;
import com.project.testproject.mvp.models.Country;
import com.project.testproject.mvp.models.General;
import com.project.testproject.mvp.presenters.CountryPresenter;
import com.project.testproject.mvp.views.CountryView;

import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by onuchin on 19.11.2016.
 */

public class CountryFragment extends MvpAppCompatFragment implements CountryView {
    @InjectPresenter
    CountryPresenter countryPresenter;
    List<MultiCheckCountry> countries;

    private MultiCheckCountryAdapter adapter;
    private Unbinder unbinder;
     public ProgressBar progressView;
    public General general;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.country_fragment, container, false);
       unbinder=ButterKnife.bind(this,v);
        return v;

    }
    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
         progressView =(ProgressBar)view.findViewById(R.id.login_progress);

        countryPresenter.requestListcountry();
        adapter = new MultiCheckCountryAdapter();
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }



    public void makeMulticheckCountry(String countryName, List<City> citiesOfCountry) {
        MultiCheckCountry country = new MultiCheckCountry(countryName, citiesOfCountry);
        countries.add(country);
    }
    public  void forAdapter(List<City> cities){


    }

    @Override
    public void successRequest() {

    }

    @Override
    public void showProgress() {
        progressView.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgress() {
 progressView.setVisibility(View.GONE);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(getActivity().getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideError() {

    }
}
