package com.project.testproject.app;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;

/**
 * Created by onuchin on 17.11.2016.
 */

public class AzureError extends  Throwable {


    public AzureError(ResponseBody responseBody){
        super(getMessage(responseBody));
    }
    public static  String getMessage(ResponseBody responseBody){
        try{
            return  new JSONObject(responseBody.string()).optString("message");
        }
        catch (JSONException| IOException e){
            e.printStackTrace();
        }
        return  "Uknown exception";
    }
}
