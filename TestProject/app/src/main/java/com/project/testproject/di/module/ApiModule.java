package com.project.testproject.di.module;

import com.project.testproject.app.OpenApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by onuchin on 19.11.2016.
 */

@Module(includes = {RetrofitModule.class})
public class ApiModule {
    @Provides
    @Singleton
    public OpenApi provideAuthApi(Retrofit retrofit) {
        return retrofit.create(OpenApi.class);
    }

}
