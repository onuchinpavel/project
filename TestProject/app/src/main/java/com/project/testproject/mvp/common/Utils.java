package com.project.testproject.mvp.common;

import com.project.testproject.app.AzureError;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by onuchin on 19.11.2016.
 */

public class Utils {

    public static <T>Observable<T> wrapRetrofitCall(final Call<T> call){
        return Observable.create(subscriber -> {

            final Response<T> execute;
            try {
                execute=call.execute();
            }
            catch (IOException e){
                subscriber.onError(e);
                return;
            }
            if(execute.isSuccess()){
                subscriber.onNext(execute.body());
            }
            else {
                subscriber.onError(new AzureError(execute.errorBody()));
            }
        });
    }
    public  static <T>Observable<T> wrapSync(Observable<T> observable, Scheduler  io){
        return  wrapSync(observable,io);
    }
    public static <T> Observable<T> wrapAsync(Observable<T> observable, Scheduler scheduler) {
        return observable
                .subscribeOn(scheduler)
                .observeOn(AndroidSchedulers.mainThread());
    }
}
