package com.project.testproject.mvp.views;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

/**
 * Created by onuchin on 17.11.2016.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface CountryView extends MvpView {
    @StateStrategyType(SkipStrategy.class)
    void successRequest();
  void showProgress();
    void hideProgress();
    void showError(String error);
    void hideError();

}
