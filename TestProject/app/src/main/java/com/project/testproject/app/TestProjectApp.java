package com.project.testproject.app;

import android.app.Application;

import com.project.testproject.di.component.AppComponent;
import com.project.testproject.di.component.DaggerAppComponent;
import com.project.testproject.di.module.ContextModule;

/**
 * Created by onuchin on 17.11.2016.
 */

public class TestProjectApp extends Application {
 private  static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent= DaggerAppComponent.builder().contextModule(new ContextModule(this))
                .build();
    }
    public  static AppComponent getAppComponent(){ return appComponent;}
}
