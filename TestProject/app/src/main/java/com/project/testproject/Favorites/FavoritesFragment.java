package com.project.testproject.Favorites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.project.testproject.R;

/**
 * Created by onuchin on 17.11.2016.
 */

public class FavoritesFragment extends MvpAppCompatFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.favorite_fragment,container,false);
        return  v;
    }
}
