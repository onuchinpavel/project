package com.project.testproject.di.module;

import com.project.testproject.app.OpenApi;
import com.project.testproject.app.OpenApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by onuchin on 19.11.2016.
 */
@Module(includes = {ApiModule.class})
public class OpenApiModule {
    @Provides
    @Singleton
    public OpenApiService provideOpenApiService(OpenApi openApi){
        return  new OpenApiService(openApi);
    }
}
