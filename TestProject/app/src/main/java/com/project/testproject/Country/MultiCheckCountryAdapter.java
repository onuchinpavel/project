package com.project.testproject.Country;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.testproject.R;
import com.project.testproject.mvp.models.City;
import com.thoughtbot.expandablecheckrecyclerview.CheckableChildRecyclerViewAdapter;
import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by onuchin on 19.11.2016.
 */

public class MultiCheckCountryAdapter extends CheckableChildRecyclerViewAdapter<CountryViewHolder,MultiCheckCityViewHolder> {

    public MultiCheckCountryAdapter(List<? extends CheckedExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public MultiCheckCityViewHolder onCreateCheckChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_multicheck_city,parent,false);
        return  new MultiCheckCityViewHolder(view);
    }

    @Override
    public void onBindCheckChildViewHolder(MultiCheckCityViewHolder holder, int flatPosition, CheckedExpandableGroup group, int childIndex) {
final City city =(City)group.getItems().get(childIndex);
        holder.setCityName(city.getName());
    }

    @Override
    public CountryViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_genre,parent,false);
        return  new CountryViewHolder(view);
    }

    @Override
    public void onBindGroupViewHolder(CountryViewHolder holder, int flatPosition, ExpandableGroup group) {
holder.setGenreTitle(group);
    }
}
