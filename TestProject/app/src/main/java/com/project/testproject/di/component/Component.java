package com.project.testproject.di.component;

import android.content.Context;

import com.project.testproject.app.OpenApi;
import com.project.testproject.di.module.ApiModule;
import com.project.testproject.di.module.BusModule;
import com.project.testproject.di.module.ContextModule;
import com.project.testproject.di.module.OpenApiModule;
import com.project.testproject.mvp.presenters.CountryPresenter;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

/**
 * Created by onuchin on 19.11.2016.
 */

@Singleton
@dagger.Component(modules = {ContextModule.class, OpenApiModule.class, BusModule.class})
public interface Component {
    Context getContext();
    OpenApi getAuthService();
    Bus getBus();
    void inject(CountryPresenter presenter);
}
