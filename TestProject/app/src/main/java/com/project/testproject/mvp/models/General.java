package com.project.testproject.mvp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

/**
 * Created by onuchin on 19.11.2016.
 */


    @Generated("org.jsonschema2pojo")
    public class General {

        @SerializedName("Timestamp")
        @Expose
        private String timestamp;
        @SerializedName("Error")
        @Expose
        private Object error;
        @SerializedName("Result")
        @Expose
        private List<Country> result = new ArrayList<Country>();

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public List<Country> getResult() {
        return result;
    }

    public void setResult(List<Country> result) {
        this.result = result;
    }
}
