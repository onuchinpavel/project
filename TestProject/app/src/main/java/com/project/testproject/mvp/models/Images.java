package com.project.testproject.mvp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by onuchin on 19.11.2016.
 */

public class Images implements Parcelable{
    public Images() {
    }

    @SerializedName("ImageLink")
    @Expose
    private String imagelink;
    @SerializedName("ShouldShowWatermark")
    @Expose
    private  boolean water;

    protected Images(Parcel in) {
        imagelink = in.readString();
        water = in.readByte() != 0;
    }

    public static final Creator<Images> CREATOR = new Creator<Images>() {
        @Override
        public Images createFromParcel(Parcel in) {
            return new Images(in);
        }

        @Override
        public Images[] newArray(int size) {
            return new Images[size];
        }
    };

    public String getImagelink() {
        return imagelink;
    }

    public void setImagelink(String imagelink) {
        this.imagelink = imagelink;
    }

    public boolean isWater() {
        return water;
    }

    public void setWater(boolean water) {
        this.water = water;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(imagelink);
        parcel.writeByte((byte) (water ? 1 : 0));
    }
}
