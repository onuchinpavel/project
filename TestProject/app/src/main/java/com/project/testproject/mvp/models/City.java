package com.project.testproject.mvp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by onuchin on 19.11.2016.
 */

public class City implements Parcelable {
    public City(String name,boolean isFavorite) {
        this.name = name;
        this.isFavorite=isFavorite;
    }
    public  City(Parcel in){
        name=in.readString();
    }

    private  boolean isFavorite;
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("CountryId")
    @Expose
    private Integer countryId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ImageLink")
    @Expose
    private Object imageLink;
    @SerializedName("Artists")
    @Expose
    private List<Artists> artists = new ArrayList<Artists>();

    public List<Artists> getArtists() {
        return artists;
    }

    public void setArtists(List<Artists> artists) {
        this.artists = artists;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getImageLink() {
        return imageLink;
    }

    public void setImageLink(Object imageLink) {
        this.imageLink = imageLink;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
parcel.writeString(name);
    }
    public static  final  Creator<City> CREATOR = new Creator<City>() {
        @Override
        public City createFromParcel(Parcel parcel) {
            return new City(parcel);
        }

        @Override
        public City[] newArray(int i) {
            return new City[0];
        }
    };
}
